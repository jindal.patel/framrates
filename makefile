# 
#  
SHELL=/bin/bash
curr_dir=$(CURDIR) 

# Top level command, will build and run unit tests
build: prep_artifacts tests  

# Clean all artifacts
clean:

	@echo 'Cleaning Derived Data'
	rm -rf ~/Library/Developer/Xcode/DerivedData/*
	@echo 'Clean Build Artifacts'
	rm -rf FramRates/build/
	mkdir -p FramRates/build
	@echo 'Cleaning Intermediates' 
	rm -rf /tmp/ios_build/*

# Build framework and prepare artifacts
framework: clean

	@echo 'Building Framework'
	xcodebuild -version
	xcodebuild -scheme FramRates

prep_artifacts: framework 

	@echo 'Copying framework and test artifacts to local directories'

	@echo 'Zipping Framework'
	cd FramRates/build/Build/Products/Release-iphoneos/ ; \
	tar -cvzf FramRates.tgz  FramRates.framework 
	cp FramRates/build/Build/Products/Release-iphoneos/FramRates.tgz FramRates/build

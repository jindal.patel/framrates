#utils.py

"""
See REL_ENG_README.txt
"""

import argparse
import os
import sys
from subprocess import check_call, check_output, CalledProcessError, Popen, PIPE

file_location = os.path.dirname(os.path.realpath(__file__))
push_location = os.path.realpath(os.path.join(file_location, 'push'))
build_location = os.path.realpath(os.path.join(file_location, '..', 'FramRates', 'build'))
workspace_location = os.path.realpath(os.path.join(file_location, '..', 'FramRates'))

# global parser
parser = ''

def shell_call(call, location, shell_flag=None, output=False):
    if output:
        shell_func_call = check_output
    else:
        shell_func_call = check_call

    try:
        if shell_flag:
            result = shell_func_call(call, cwd=location, shell=True)
        else:
            result = shell_func_call(call, cwd=location)
        print "Exited with status %s" % result
        if output:
            return result 
    except CalledProcessError, cpe:
        # TODO: Logging
        print "Called process exception"
        raise cpe

def pipe_call(call, location, shell_flag=False, env=None):
    if env:
        process = Popen(call, shell=shell_flag, stdout=PIPE, cwd=location, env=env)
    else:
        process = Popen(call, shell=shell_flag, stdout=PIPE, cwd=location)
    stdout_list = process.communicate()[0].split('\n')
    return stdout_list

def copy_framework():
    call = ['xcodebuild', '-xcodeproj', 'FramRates.xcodeproj', '-scheme', 'Framework', '-showBuildSettings']
    out = pipe_call(call, workspace_location)
    build_dir = ''
    for line in out:
        if ' BUILD_DIR ' in line:
            build_dir = line
    build_dir = build_dir.split(' ')[-1]
    build_dir = build_dir.split('/Build/Products')[0]
    cp_call = ['cp', '-R', build_dir+'/', build_location]
    shell_call(cp_call, build_location)

def run_unit():
    #call = ['SIMCTL_CHILD_GHUNIT_AUTORUN=YES', 'SIMCTL_CHILD_WRITE_JUNIT_XML=YES', 'SIMCTL_CHILD_JUNIT_XML_DIR='+build_location, 'xcrun', 'simctl', 'launch', 'booted', 'com.splunk.mint.test.splunk.SplunkTests']
    call = ['xcrun', 'simctl', 'launch', 'booted', 'com.splunk.mint.test.splunk.SplunkTests']
    call = "xcrun simctl launch booted com.splunk.mint.test.splunk.SplunkTests"
    #SIMCTL_CHILD_GHUNIT_AUTORUN=YES   SIMCTL_CHILD_WRITE_JUNIT_XML=YES SIMCTL_CHILD_JUNIT_XML_DI=curr_dir xcrun simctl launch booted com.splunk.mint.test.splunk.SplunkTests
    print call 
    my_env = os.environ.copy()
    my_env['SIMCTL_CHILD_GHUNIT_AUTORUN'] = 'YES'
    my_env['SIMCTL_CHILD_WRITE_JUNIT_XML'] = 'YES'
    my_env['SIMCTL_CHILD_JUNIT_XML_DIR'] = build_location
    out = pipe_call(call, file_location, shell_flag=True, env=my_env)
    print out


def parse():
    global parser
    # top parser
    parser = argparse.ArgumentParser(prog='iOS CI utilities', description='Utility Command line tool for building the ios SDK')
    # copy built framework files to local build directory
    parser.add_argument("--utility", type=str, default=None, required=True, help="utility name you wish to run: [copy_framework]")
    return parser.parse_args()


def main():
    global parser
    # Parse out the options, and execute for "help"
    args = parse()
    # select the function to run
    print args.__dict__
    chain = {
            # Swith Statement for the utility names
            # the key is the argument passed into -utitily
            # the value is the function to be executed
            'copy_framework': copy_framework,
            'run_unit': run_unit

        }.get(args.utility, parser.print_help)
    # execute the function
    chain()


if __name__ == "__main__":
    main()
